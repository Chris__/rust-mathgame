# Math Game in Rust
This repository contains a tiny console game in which the goal is to solve simple math exercises. It's the first time I programmed something in Rust and the goal was to try out some concepts in practice.

## License
This software is licensed under the MIT license which can be found in the 