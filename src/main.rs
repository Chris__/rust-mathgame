// SPDX-License-Identifier: MIT

use std::{fmt, io};
use std::io::Write;
use rand::distributions::{Distribution, Standard};
use rand::Rng;

// First derive directive makes it possible to convert from a number to an enum value
// using num::FromPrimitive::from_u32
#[derive(num_derive::FromPrimitive, Copy, Clone)]
enum MathematicOperation {
    ADD = 0,
    SUBTRACT = 1,
    MULTIPLY = 2,
}

// Implement a trait to facilitate representing the enum values as text when formatting
impl fmt::Display for MathematicOperation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let operator_string;
        match self {
            MathematicOperation::ADD => operator_string = "+",
            MathematicOperation::SUBTRACT => operator_string = "-",
            MathematicOperation::MULTIPLY => operator_string = "*",
        };
        write!(f, "{}", operator_string)
    }
}

// Facilitate random generation of MathematicOperation enum value
// using rand::thread_rng().gen::<MathematicOperation>();
impl Distribution<MathematicOperation> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> MathematicOperation {
        let mathematic_op = num::FromPrimitive::from_u32(rng.gen_range(0..=2));
        mathematic_op.expect("Random number out of range")
    }
}

fn main() {
    println!("Let's play a game! I'm gonna ask you to find the solution of simple mathematical calculations using the operators +,- and *.\n\
    Are you ready? (Y/n)");
    let mut buffer = String::with_capacity(20);
    io::stdin().read_line(&mut buffer).expect("Couldn't read line!");
    if buffer.chars().next().unwrap_or(' ') != 'Y'{
        return;
    }
    println!("Then let's start!");
    for _i in 0..=3 {

        let operation = rand::thread_rng().gen::<MathematicOperation>();

        // Generate the two numbers
        let mut numbers: [i16; 2] = [0, 0];
        let upper_number_limit = match operation {
            MathematicOperation::ADD | MathematicOperation::SUBTRACT => {
                50
            }
            MathematicOperation::MULTIPLY => {
                20
            }
        };
        numbers.iter_mut().for_each(|x| *x = rand::thread_rng().gen_range(0..=upper_number_limit));

        let result = execute_mathematic_operation(operation, (numbers[0],numbers[1]));

        // For example: 2 + 2 = ?
        print!("{} {} {} = ", numbers[0], operation, numbers[1]);
        io::stdout().flush().expect("Failed to flush stdout");

        if read_number_from_stdin(&mut buffer) == result {
            println!("Amazing! Your solution is correct");
        } else {
            println!("Sorry, the actual result is {}", result);
        }
    }
}
fn execute_mathematic_operation(mathematic_op: MathematicOperation, numbers: (i16,i16)) -> i16 {
    match mathematic_op {
        MathematicOperation::ADD => {
            return numbers.0 + numbers.1;
        }
        MathematicOperation::SUBTRACT => {
            return numbers.0 - numbers.1;
        }
        MathematicOperation::MULTIPLY => {
            return numbers.0 * numbers.1;
        }
    };
}
fn read_number_from_stdin(mut buffer: &mut String) -> i16 {
    buffer.clear();
    io::stdin().read_line(&mut buffer).expect("Couldn't read frin stdin!");
    buffer.trim_end() // Remove new line at end
        .parse::<i16>()
        .unwrap_or(0)
}